package com.example.himanshusingh.assignment2;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

         USERNAME = (EditText)findViewById(R.id.username) ;
        PASSWORD = (EditText)findViewById(R.id.password);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





    EditText USERNAME ;
    EditText  PASSWORD ;

    String  keySuccess = "success";


    boolean ResponseSuccess;

    public void  login (final View view){

        String url;
        String id = USERNAME.getText().toString();
        String pass = PASSWORD.getText().toString();
        url = "http://10.250.215.13:8000/default/login.json?userid=cs1110200&password=john";


        StringRequest sr = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
            @Override
            //extracts the response message from the response string
            //accordingly genearates message on the sanckbar



            public void onResponse(String response) {
                try {
                    JSONObject reply = new JSONObject(response);
                    ResponseSuccess = reply.getBoolean(keySuccess);
                    Snackbar.make(view, "true", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }catch(JSONException e)
                {
                    Snackbar.make(view, "catchexception", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override

            public void onErrorResponse(VolleyError error) {
                Snackbar.make(view, "error", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        RequestQueue queue =  Volley.newRequestQueue(this);
        queue.add(sr);


    }




}